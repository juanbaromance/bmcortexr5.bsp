var searchData=
[
  ['sbus',['sbus',['../structlinux__bus.html#a213e122d9780d9ffb83cd84993b9c8e8',1,'linux_bus']]],
  ['sdev',['sdev',['../structlinux__device.html#a47abb7026acf68a8de75135e2a08fc21',1,'linux_device']]],
  ['sdrv',['sdrv',['../structlinux__driver.html#afce51f414c82ebf42eb22d778609bf8b',1,'linux_driver']]],
  ['sg',['sg',['../structmetal__shm__ref.html#a6b66c7592280874bd3788d54abd648f9',1,'metal_shm_ref::sg()'],['../structmetal__generic__shmem.html#a53c5b5115b3c1e40def923a22184d580',1,'metal_generic_shmem::sg()']]],
  ['size',['size',['../structmetal__io__region.html#a131ebc3df546c6e87b1f833882d9fc1e',1,'metal_io_region::size()'],['../structmetal__generic__shmem.html#acd050cc8d19f8fdc01bc04c6b568a180',1,'metal_generic_shmem::size()'],['../structuio__dmabuf__args.html#ab3a985c171b72c48d07fc5d1a78becc5',1,'uio_dmabuf_args::size()']]],
  ['sync_5ffor_5fcpu',['sync_for_cpu',['../structmetal__shm__ops.html#a4ff1e8cf550bdb6fa555856279717346',1,'metal_shm_ops']]],
  ['sync_5ffor_5fdevice',['sync_for_device',['../structmetal__shm__ops.html#a9e06f7b5a7a32b7c2ea43c03cc2afd88',1,'metal_shm_ops']]],
  ['sysfs_5fpath',['sysfs_path',['../structmetal__state.html#adb50fd0d288917778f721619dcaba32e',1,'metal_state']]]
];
