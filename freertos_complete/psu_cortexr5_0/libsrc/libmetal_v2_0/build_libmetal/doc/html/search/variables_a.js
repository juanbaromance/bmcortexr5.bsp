var searchData=
[
  ['m',['m',['../structmetal__condition.html#a475d6e0ebf1a3600983ce0f51e7cc83c',1,'metal_condition']]],
  ['mem_5fflags',['mem_flags',['../structmetal__io__region.html#a457b7d189af94966273824a9a95b1cc1',1,'metal_io_region']]],
  ['metal_5fgeneric_5fbus',['metal_generic_bus',['../group__device.html#gab25f549ea3a9151e82c033dd219bbfe5',1,'metal_generic_bus():&#160;device.c'],['../group__device.html#gab25f549ea3a9151e82c033dd219bbfe5',1,'metal_generic_bus():&#160;device.c']]],
  ['metal_5fio_5fphys_5fstart_5f',['metal_io_phys_start_',['../system_2nuttx_2io_8c.html#a5ffdedfa11ebef9305f511b90ae5d9bb',1,'io.c']]],
  ['metal_5fio_5fregion_5f',['metal_io_region_',['../system_2nuttx_2io_8c.html#a7606f5b17ae71385edb0c0845a01f61c',1,'io.c']]],
  ['metal_5flinux_5fbus_5fops',['metal_linux_bus_ops',['../system_2linux_2device_8c.html#a8fa541afe7299c15be47e4959f36c782',1,'device.c']]],
  ['metal_5flinux_5fshm_5fops',['metal_linux_shm_ops',['../shmem-provider-shm_8c.html#ab4d7368432bb14543bd45227020bc21b',1,'shmem-provider-shm.c']]],
  ['metal_5fshm_5fdma_5fbuf_5fops',['metal_shm_dma_buf_ops',['../shmem-dma_8c.html#ab7e4ec9865694c21bd510e1e61c3ae56',1,'metal_shm_dma_buf_ops():&#160;shmem-dma.c'],['../shmem-provider-ion_8c.html#ab7e4ec9865694c21bd510e1e61c3ae56',1,'metal_shm_dma_buf_ops():&#160;shmem-dma.c']]],
  ['metal_5fsoftirq_5favail',['metal_softirq_avail',['../softirq_8c.html#a470e1baa97a9b3e6a31a112867694060',1,'softirq.c']]],
  ['mmap',['mmap',['../structmetal__shm__ops.html#a5b52187db8f427348a6d76807cddac57',1,'metal_shm_ops']]],
  ['mmap_5fflags',['mmap_flags',['../structmetal__page__size.html#ad397236c18313e189cb99b31bad74750',1,'metal_page_size']]],
  ['mod_5fname',['mod_name',['../structlinux__driver.html#aa37549bd63322adbfd14e9d5f0c9bb9d',1,'linux_driver']]],
  ['munmap',['munmap',['../structmetal__shm__ops.html#a688373794a677c9fda92c534cf5f93c6',1,'metal_shm_ops']]]
];
