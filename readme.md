### Building Tweaks 

* Xilinx SDK prepares the *Makefiles* such as compilation directories named as **build_libmetal** and **build_openamp** are expected on the *psu_cortexr5_0/libsrc/libmetal_vx_y* and  *psu_cortexr5_0/libsrc/openamp_vx_y* directories

* The **open-amp** amd **libmetal** packages are implemented with **cmake**, apply below recipe on the building directories afterwards sources installation in your host. 

```
cmake ../src/libmetal/ -DCMAKE_TOOLCHAIN_FILE=../src/libmetal/cmake/platforms/toolchain.cmake
cmake ../src/open-amp/ -DCMAKE_TOOLCHAIN_FILE=../src/open-amp/cmake/platforms/toolchain.cmake
```

* Before making anything, assure a proper overloading of your execution path with the Xilinx SDK R5 baremetal toolchain

```
jb@popov $ export PATH=$PATH:*YourSDKRoot*/2019.2/tools/xsct/gnu/armr5/lin/gcc-arm-none-eabi/bin/  
```

* The **prebuild** directory contains the headers and static libraries of the pre-compiled packages ready to be used. The .mss (microprocessor software specification) provides the specific versions for the every component of the .bsp (board support package)

* We should consider the posibility to store the *.bsp* variants using *git* branches
