
set (BSP_ROOT_DIR  "/opt/development/zynq/Xilinx/BareMetal/CortexR5/BMCortexR5.bsp/freertos_openamp/psu_cortexr5_0")
set (CMAKE_SYSTEM_PROCESSOR "arm" CACHE STRING "")
set (MACHINE "zynqmp_r5")
set (CROSS_PREFIX "armr5-none-eabi-" CACHE STRING "")
set (CMAKE_C_FLAGS 
       "-O2 -c -mcpu=cortex-r5 -g -DARMR5 -Wno-implicit-function-declaration -Wno-stringop-truncation -Wall -Wextra -mfloat-abi=hard -mfpu=vfpv3-d16 -DUNDEFINE_FILE_OPS -I${BSP_ROOT_DIR}/include " 
	   CACHE STRING "")

set (CMAKE_SYSTEM_NAME "FreeRTOS" CACHE STRING "")

# include (CMakeForceCompiler)
SET (CMAKE_C_COMPILER_WORKS 1)
SET (CMAKE_CXX_COMPILER_WORKS 1)
set (CMAKE_C_COMPILER "${CROSS_PREFIX}gcc" )
set (CMAKE_CXX_COMPILER "${CROSS_PREFIX}g++")

set (LIBMETAL_INCLUDE_DIR ${BSP_ROOT_DIR}/include/metal )
set (LIBMETAL_LIB_DIR ${BSP_ROOT_DIR}/lib )
set (LIBMETAL_LIB ${BSP_ROOT_DIR}/libmetal )

set (CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER CACHE STRING "")
set (CMAKE_FIND_ROOT_PATH_MODE_LIBRARY NEVER CACHE STRING "")
set (CMAKE_FIND_ROOT_PATH_MODE_INCLUDE NEVER CACHE STRING "")
